# TEMPer

A Ruby library for accessing USB sensor "TEMPer" [0c45:7401].

Based on [temper-python][1] by Philipp Adelt.

## Usage

Build and install the gem:

```
$ gem build temper.gemspec
$ gem install temper-0.2.0.gem
```

Then you can run the CLI:

```
$ temper --plain
28.9375
```

### Calibration

These "TEMPer" devices are not very accurate, to say the least. I own three of them and the measured values differ by several degrees Celsius. So, first thing to do: get some reference thermometer, put it and your TEMPer device at the exact same place, wait some time to let it stabilize, and the measured difference is your offset for this single device. This does compensate for drift only, not pitch variation or non-linearity.

There is no configuration file, so add your offset as command argument, for example:

```
$ temper --offset=1.35
```

If you attached more than one TEMPer, you have to provide an offset value for each device. As TEMPer devices do not have unique serial numbers, check the usb bus path with `temper --list`.

For example:

```
$ temper --list
Device 1: 0c45:7401 @ 1/5
Device 2: 0c45:7401 @ 1/6
$ temper --offset 1/5:1.35 --offset 1/6:-0.7
```

## Permissions

To be able to use `temper` as a non-root user, you may have to add an udev rule file (e.g. `/etc/udev/rules.d/99-temper.rules`):

```
SUBSYSTEM=="usb", ACTION=="add", ATTR{idVendor}=="0c45", ATTR{idProduct}=="7401", MODE="666"
```

On my Raspberry Pi I found that after some time the TEMPer device stopped responding. I had to add `usbhid.quirks=0x0c45:0x7401:0x4` to `/boot/cmdline.txt` to prevent this. You will find more details [here](https://github.com/padelt/temper-python#tell-kernel-to-leave-temper-alone) on this matter. After that I had measuring faults occasionally. To compensate this for each measurement three values will be read and only the median value will be printed.


## References

* [temper-python][1] by Philipp Adelt.
* See also [this][2] and [that][3].

[1]: https://github.com/padelt/temper-python
[2]: http://www.isp-sl.com/pcsensor-1.0.0.tgz
[3]: https://github.com/bitplane/temper
