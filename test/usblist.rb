require 'libusb'

usb = LIBUSB::Context.new
usb.devices.each_with_index do |dev, i|
    puts "Device #{i}: #{dev.inspect}"
end

# vim: set et sw=4 ts=4:
