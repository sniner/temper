# Copyright (c) 2017, Stefan Schoenberger <stefan.schoenberger@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.


# Ruby library for accessing USB sensor 'TEMPer' [0c45:7401]
#
# Based on temper-python [1] by Philipp Adelt
#
# See also:
#  [1] https://github.com/padelt/temper-python
#  [2] http://www.isp-sl.com/pcsensor-1.0.0.tgz
#  [3] https://github.com/bitplane/temper

require 'libusb'

module TEMPer

    VENDOR = 0x0c45
    PRODUCT = 0x7401

    def self.devices
        usb = LIBUSB::Context.new
        @devices = usb.devices(:idVendor => VENDOR, :idProduct => PRODUCT).map do |dev|
            Device.new(dev)
        end
    end

    class Device
        def initialize(dev)
            @device = dev
        end

        def open
            @endpoints_in = @device.endpoints.select{|ep| ep.bEndpointAddress&LIBUSB::ENDPOINT_IN != 0}
            @endpoints_out = @device.endpoints.select{|ep| ep.bEndpointAddress&LIBUSB::ENDPOINT_IN == 0}

            @handle = @device.open
            if LIBUSB.has_capability? LIBUSB::CAP_SUPPORTS_DETACH_KERNEL_DRIVER
                @device.interfaces.each do |i|
                    @handle.detach_kernel_driver(i) if @handle.kernel_driver_active?(i)
                end
            end
            @device.interfaces.each {|i| @handle.claim_interface(i)}

            ini_ctrl_write

            if block_given? && open?
                begin
                    yield self
                ensure
                    close
                end
            end

            self
        end

        def open?
            ! @handle.nil?
        end

        def close
            if open?
                @device.interfaces.each {|i| @handle.release_interface(i)}
                if LIBUSB.has_capability? LIBUSB::CAP_SUPPORTS_DETACH_KERNEL_DRIVER
                    @device.interfaces.each {|i| @handle.attach_kernel_driver(i)}
                end
                @handle.close
                @handle = nil
            end
        end

        def reset
            was_open = open?
            if was_open
                @device.interfaces.each {|i| @handle.release_interface(i)}
            else
                @handle = @device.open
            end

            @handle.reset_device
            @handle.close
            @handle = nil

            open if was_open
        end

        def name
            @device.product
        end

        def product
            @device.idProduct
        end

        def vendor
            @device.idVendor
        end

        def manufacturer
            @device.manufacturer
        end

        # TEMPer devices do not have a unique serial number, so this value
        # is pretty worthless. To identify multiple devices use #dev_path
        # instead.
        def serial_number
            @device.idSerialNumber
        end

        def id
            sprintf("%04x:%04x", @device.idVendor, @device.idProduct)
        end

        def dev_path
            "#{@device.bus_number}/#{@device.device_address}"
        end

        def ini_ctrl_write
            len = @handle.control_transfer(
                    :bmRequestType => LIBUSB::ENDPOINT_OUT|LIBUSB::REQUEST_TYPE_CLASS|LIBUSB::RECIPIENT_INTERFACE,
                    :bRequest => 0x09,
                    :wValue => 0x201,
                    :wIndex => 0x00,
                    :dataOut => [0x01, 0x01].pack('C*'))
            len == 2
        end

        def ctrl_write(*data)
            len = @handle.control_transfer(
                    :bmRequestType => LIBUSB::ENDPOINT_OUT|LIBUSB::REQUEST_TYPE_CLASS|LIBUSB::RECIPIENT_INTERFACE,
                    :bRequest => 0x09,
                    :wValue => 0x200,
                    :wIndex => 0x01,
                    :dataOut => data.pack('C*'))
            len == data.length
        end

        def intr_read(bytes, timeout=1)
            ep = @endpoints_in[1]
            return nil unless ep
            retries = 3
            begin
                @handle.interrupt_transfer(
                    :endpoint => ep,
                    :dataIn => bytes || ep.wMaxPacketSize,
                    :timeout => timeout*1000)
            rescue LIBUSB::ERROR_PIPE
                @handle.clear_halt(ep)
                if (retries -= 1)>0
                    retry
                end
            rescue LIBUSB::Error => ex
                $stderr.puts ex
                nil
            end
        end

        COMMANDS = {
            temp: [0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00],
            ini1: [0x01, 0x82, 0x77, 0x01, 0x00, 0x00, 0x00, 0x00],
            ini2: [0x01, 0x86, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00],
        }

        def single_raw_value
            ctrl_write(*COMMANDS[:temp])
            intr_read(8)
            ctrl_write(*COMMANDS[:ini1])
            intr_read(8)
            ctrl_write(*COMMANDS[:ini2])
            intr_read(8)
            intr_read(8)
            ctrl_write(*COMMANDS[:temp])
            d = intr_read(8)
            if d && d.length==8
                s = d.unpack('C*')
                s[3] + s[2]*256
            end
        end

        # To compensate false readings three values are read
        # and the median value will be returned.
        def raw_value
            n = 3 # values needed (must be odd number)

            # It happens that the temper device does not
            # answer, so we try a number of times to read
            # the needed values before giving up.
            values = []
            count = 0
            loop do
                v = single_raw_value
                values << v if v
                break if values.length == n
                count += 1
                raise "Unable to read data: #{self}" if count>10
            end

            values.sort.at(n/2)
        end

        # Get current temperature reading (Celsius)
        def value
            raw_value*125.0/32000.0
        end

        alias :temperature :value
        alias :celsius :value

        # Get current temperature reading (Fahrenheit)
        def fahrenheit
            celsius*1.8 + 32.0
        end

        def to_s
            "#{self.name} (#{self.id}) by #{self.manufacturer}"
        end
    end
end


# vim: et sw=4 ts=4
