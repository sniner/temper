Gem::Specification.new do |s|
    s.required_ruby_version = '>= 1.9.3'

    s.name        = 'temper'
    s.version     = '0.2.0'
    s.date        = '2017-08-14'
    s.summary     = 'Access TEMPer devices'
    s.description = 'Ruby library for accessing USB sensor "TEMPer" [0c45:7401]'
    s.authors     = ['Stefan Schönberger']
    s.email       = 'stefan.schoenberger@gmail.com'
    s.homepage    = 'https://github.com/sniner/temper/'
    s.license     = 'LGPL-3.0'
    s.files       = Dir.glob('lib/*.rb') + Dir.glob('bin/*')
    s.executables << 'temper'
    s.add_runtime_dependency 'libusb', '~> 0.6'
end

